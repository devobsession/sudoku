using Sudoku.Core;
using System;
using System.Linq;
using Xunit;

namespace Sudoku.UnitTests
{
    public class NumberOfBlocksWithSameNumberOfPossibilitiesReducerTests
    {
        [Fact]
        public void WhenExistingsAnswersConflictWithPossibilities_RemoveTheConflictingPossibilities()
        {
            // Arrange
            var puzzleArr = new int[]
             {
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0
             };
            Puzzle puzzle = new Puzzle(puzzleArr);
            puzzle.AddPossibility(0,0, 1);
            puzzle.AddPossibility(0,0, 2);
            puzzle.AddPossibility(0,0, 3);

            puzzle.AddPossibility(0,1, 3);

            puzzle.AddPossibility(0,2, 1);
            puzzle.AddPossibility(0,2, 2);

            NumberOfBlocksWithSameNumberOfPossibilitiesReducer reducer = new NumberOfBlocksWithSameNumberOfPossibilitiesReducer();

            // Act
            reducer.Reduce(puzzle);

            // Assert
            Assert.True(1 == puzzle.GetPossibilities(0,0).SingleOrDefault(x => x == 1));
            Assert.True(2 == puzzle.GetPossibilities(0,0).SingleOrDefault(x => x == 2));
            Assert.False(3 == puzzle.GetPossibilities(0,0).SingleOrDefault(x => x == 3));
        }
    }
}
