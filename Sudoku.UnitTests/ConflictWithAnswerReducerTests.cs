using Sudoku.Core;
using System;
using System.Linq;
using Xunit;

namespace Sudoku.UnitTests
{
    public class ConflictWithAnswerReducerTests
    {
        [Fact]
        public void WhenExistingsAnswersConflictWithPossibilities_RemoveTheConflictingPossibilities()
        {
            // Arrange
            var puzzleArr = new int[]
             {
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 7,0,0, 0,0,0,
                0,0,0, 0,0,0, 7,0,0,

                0,7,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,

                0,0,7, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0
             };
            Puzzle puzzle = new Puzzle(puzzleArr);
            puzzle.AddPossibility(0,0, 7);
            puzzle.AddPossibility(0,1, 7);
            puzzle.AddPossibility(0,2, 7);
            puzzle.AddPossibility(1,0, 7);
            puzzle.AddPossibility(1,1, 7);
            puzzle.AddPossibility(1,2, 7);
            puzzle.AddPossibility(2,0, 7);
            puzzle.AddPossibility(2,1, 7);
            puzzle.AddPossibility(2,2, 7);

            ConflictWithAnswerReducer reducer = new ConflictWithAnswerReducer();

            // Act
            reducer.Reduce(puzzle);

            // Assert
            Assert.True(7 == puzzle.GetPossibilities(0,0).SingleOrDefault(x => x == 7));
            Assert.False(7 == puzzle.GetPossibilities(0,1).SingleOrDefault(x => x == 7));
            Assert.False(7 == puzzle.GetPossibilities(0,2).SingleOrDefault(x => x == 7));
            Assert.False(7 == puzzle.GetPossibilities(1,0).SingleOrDefault(x => x == 7));
            Assert.False(7 == puzzle.GetPossibilities(1,1).SingleOrDefault(x => x == 7));
            Assert.False(7 == puzzle.GetPossibilities(1,2).SingleOrDefault(x => x == 7));
            Assert.False(7 == puzzle.GetPossibilities(2,0).SingleOrDefault(x => x == 7));
            Assert.False(7 == puzzle.GetPossibilities(2,1).SingleOrDefault(x => x == 7));
            Assert.False(7 == puzzle.GetPossibilities(2, 2).SingleOrDefault(x => x == 7));

        }
    }
}
