using Sudoku.Core;
using System;
using System.Linq;
using Xunit;

namespace Sudoku.UnitTests
{
    public class StraightLinePossibilityReducerTests
    {
        [Fact]
        public void WhenPossibilitiesLineUpInABlockRow_RemoveFromRestOfRow()
        {
            // Arrange
            var puzzleArr = new int[]
             {
                0,0,0, 0,0,0, 0,0,0,
                1,2,3, 0,0,0, 0,0,0,
                4,5,6, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0
             };
            Puzzle puzzle = new Puzzle(puzzleArr);
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    puzzle.AddPossibility(y, x, 7);
                }
            }
            puzzle.RemovePossibility(1, 0, 7);
            puzzle.RemovePossibility(1, 1, 7);
            puzzle.RemovePossibility(1, 2, 7);
            puzzle.RemovePossibility(2, 0, 7);
            puzzle.RemovePossibility(2, 1, 7);
            puzzle.RemovePossibility(2, 2, 7);

            StraightLinePossibilityReducer reducer = new StraightLinePossibilityReducer();

            // Act
            reducer.Reduce(puzzle);

            // Assert
            for (int x = 3; x < 9; x++)
            {
                Assert.False(7 == puzzle.GetPossibilities(0, x).SingleOrDefault(x => x == 7));
            }

            for (int y = 1; y < 3; y++)
            {
                for (int x = 3; x < 9; x++)
                {
                    Assert.True(7 == puzzle.GetPossibilities(y, x).SingleOrDefault(x => x == 7));
                }
            }

            for (int y = 3; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    Assert.True(7 == puzzle.GetPossibilities(y, x).SingleOrDefault(x => x == 7));
                }
            }
        }

        [Fact]
        public void WhenPossibilitiesLineUpInABlockColumn_RemoveFromRestOfColumn()
        {
            // Arrange
            var puzzleArr = new int[]
             {
                0,1,2, 0,0,0, 0,0,0,
                0,3,4, 0,0,0, 0,0,0,
                0,5,6, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0
             };
            Puzzle puzzle = new Puzzle(puzzleArr);
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    puzzle.AddPossibility(y, x, 7);
                }
            }
            puzzle.RemovePossibility(0, 1, 7);
            puzzle.RemovePossibility(1, 1, 7);
            puzzle.RemovePossibility(2, 1, 7);
            puzzle.RemovePossibility(0, 2, 7);
            puzzle.RemovePossibility(1, 2, 7);
            puzzle.RemovePossibility(2, 2, 7);

            StraightLinePossibilityReducer reducer = new StraightLinePossibilityReducer();

            // Act
            reducer.Reduce(puzzle);

            // Assert
            for (int y = 3; y < 9; y++)
            {
                Assert.False(7 == puzzle.GetPossibilities(y, 0).SingleOrDefault(x => x == 7));
            }

            for (int y = 3; y < 9; y++)
            {
                for (int x = 1; x < 3; x++)
                {
                    Assert.True(7 == puzzle.GetPossibilities(y, x).SingleOrDefault(x => x == 7));
                }
            }

            for (int y = 0; y < 9; y++)
            {
                for (int x = 3; x < 9; x++)
                {
                    Assert.True(7 == puzzle.GetPossibilities(y, x).SingleOrDefault(x => x == 7));
                }
            }
        }

        [Fact]
        public void WhenPossibilitiesLineUpInABlockRowBreaks_DoNothing()
        {
            // Arrange
            var puzzleArr = new int[]
             {
                0,0,0, 0,0,0, 0,0,0,
                1,2,3, 0,0,0, 0,0,0,
                4,5,0, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0
             };
            Puzzle puzzle = new Puzzle(puzzleArr);
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    puzzle.AddPossibility(y, x, 7);
                }
            }
            puzzle.RemovePossibility(1, 0, 7);
            puzzle.RemovePossibility(1, 1, 7);
            puzzle.RemovePossibility(1, 2, 7);
            puzzle.RemovePossibility(2, 0, 7);
            puzzle.RemovePossibility(2, 1, 7);

            StraightLinePossibilityReducer reducer = new StraightLinePossibilityReducer();

            // Act
            reducer.Reduce(puzzle);

            // Assert
            for (int x = 3; x < 9; x++)
            {
                Assert.True(7 == puzzle.GetPossibilities(0, x).SingleOrDefault(x => x == 7));
            }

            for (int y = 1; y < 3; y++)
            {
                for (int x = 3; x < 9; x++)
                {
                    Assert.True(7 == puzzle.GetPossibilities(y, x).SingleOrDefault(x => x == 7));
                }
            }

            for (int y = 3; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    Assert.True(7 == puzzle.GetPossibilities(y, x).SingleOrDefault(x => x == 7));
                }
            }
        }

        [Fact]
        public void WhenPossibilitiesLineUpInABlockColumnBreaks_DoNothing()
        {
            // Arrange
            var puzzleArr = new int[]
            {
                0,1,2, 0,0,0, 0,0,0,
                0,3,4, 0,0,0, 0,0,0,
                0,5,0, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,

                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0,
                0,0,0, 0,0,0, 0,0,0
            };
            Puzzle puzzle = new Puzzle(puzzleArr);
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    puzzle.AddPossibility(y, x, 7);
                }
            }
            puzzle.RemovePossibility(0, 1, 7);
            puzzle.RemovePossibility(1, 1, 7);
            puzzle.RemovePossibility(2, 1, 7);
            puzzle.RemovePossibility(0, 2, 7);
            puzzle.RemovePossibility(1, 2, 7);

            StraightLinePossibilityReducer reducer = new StraightLinePossibilityReducer();

            // Act
            reducer.Reduce(puzzle);

            // Assert
            for (int y = 3; y < 9; y++)
            {
                Assert.True(7 == puzzle.GetPossibilities(y, 0).SingleOrDefault(x => x == 7));
            }

            for (int y = 3; y < 9; y++)
            {
                for (int x = 1; x < 3; x++)
                {
                    Assert.True(7 == puzzle.GetPossibilities(y, x).SingleOrDefault(x => x == 7));
                }
            }

            for (int y = 0; y < 9; y++)
            {
                for (int x = 3; x < 9; x++)
                {
                    Assert.True(7 == puzzle.GetPossibilities(y, x).SingleOrDefault(x => x == 7));
                }
            }
        }
    }
}
