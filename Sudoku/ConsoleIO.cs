﻿using Sudoku.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
    public class ConsoleIO
    {
        public static Puzzle BuildNewBoard()
        {
            var puzzle = new Puzzle();

            for (int y = 0; y < puzzle.Board.GetLength(0); y++)
            {
                for (int x = 0; x < puzzle.Board.GetLength(1); x++)
                {
                    while (true)
                    {
                        Display(puzzle, y, x);
                                             
                        Console.WriteLine($"input 1-9 for column:{y} and row:{x}");
                        Console.WriteLine($"0 for empty");
                        Console.WriteLine($"- for previous'");
                        Console.WriteLine($"+ for next'");
                        var key = Console.ReadKey();
                        var keyChar = key.KeyChar.ToString();
                        if (keyChar == "-")
                        {
                            if (x > 0)
                            {
                                x -= 1;
                            }
                            
                            continue;
                        }
                        else if (keyChar == "+")
                        {
                            break;
                        }

                        var parsed = int.TryParse(keyChar, out int parsedInt);
                        if (parsed)
                        {
                            if (parsedInt != 0)
                            {
                                if (puzzle.Board[y, x].Answer != null && puzzle.Board[y, x].Answer == parsedInt)
                                {
                                    break;
                                }

                                if (puzzle.IsNewAnswerAllowed(y, x, parsedInt) == false)
                                {
                                    continue;
                                }
                            }

                            puzzle.Board[y, x] = new PuzzleSpot(parsedInt);
                            break;
                        }
                    }
                }
            }

            return puzzle;
        }

        public static void Display(Puzzle puzzle, int? selectedY = null, int? selectedX = null, bool clear = true)
        {
            if (clear)
            {
                Console.Clear();
            }
            
            for (int y = 0; y < puzzle.Board.GetLength(0); y++)
            {
                for (int x = 0; x < puzzle.Board.GetLength(1); x++)
                {
                    var spot = puzzle.Board[y, x];
                    if (selectedY.HasValue && selectedX.HasValue && selectedY == y && selectedX == x)
                    {
                        Console.Write("*");
                    }
                    else if (spot?.Answer == null)
                    {
                        Console.Write("_");
                    }
                    else
                    {
                        Console.Write(spot.Answer);
                    }
                    Console.Write(" ");

                    if (x == 2 || x == 5)
                    {
                        Console.Write("  ");
                    }
                }
                if (y == 2 || y == 5)
                {
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public static bool SetAnswer(Puzzle puzzle, int y, int x, int answer)
        {
            if (puzzle.IsNewAnswerAllowed(y, x, answer))
            {
                puzzle.Board[y, x].Answer = answer;
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void DisplayPossibilities(Puzzle puzzle)
        {
            Console.WriteLine();
            Console.Write("Y:");
            int y;
            while (true)
            {
                var key = Console.ReadKey();
                var keyChar = key.KeyChar.ToString();
                var parsed = int.TryParse(keyChar, out int parsedInt);
                if (parsed)
                {
                    if (parsedInt < 9)
                    {
                        y = parsedInt;
                        break;
                    }                    
                }
            }
            Console.WriteLine();
            Console.Write("X:");
            int x;
            while (true)
            {
                var key = Console.ReadKey();
                var keyChar = key.KeyChar.ToString();
                var parsed = int.TryParse(keyChar, out int parsedInt);
                if (parsed)
                {
                    if (parsedInt < 9)
                    {
                        x = parsedInt;
                        break;
                    }
                }
            }

            Display(puzzle, y, x);
            var possibilities = puzzle.GetPossibilities(y, x);
            Console.Write("Possibilities: ");
            foreach (var possibility in possibilities)
            {
                Console.Write(possibility + " ");
            }
            Console.WriteLine();
        }

    }
}
