﻿using Sudoku.Core;
using System;
using System.Collections.Generic;

namespace Sudoku
{
    class Program
    {
        static void Main(string[] args)
        {
            // Puzzle puzzle1 = ConsoleIO.BuildNewBoard();
                        
            Puzzle puzzle = new Puzzle(1);
            ConsoleIO.Display(puzzle);

            PuzzleSolver puzzleSolver = new PuzzleSolver(puzzle, new List<IReducer>()
            { 
                new ConflictWithAnswerReducer(),
                new StraightLinePossibilityReducer(),
                new NumberOfBlocksWithSameNumberOfPossibilitiesReducer()
            });

            while (true)
            {
                Console.WriteLine("p: get possibilities");
                Console.WriteLine("any: next");
                var key = Console.ReadKey();
                if (key.KeyChar.ToString() == "p")
                {
                    ConsoleIO.DisplayPossibilities(puzzle);
                }
                else
                {
                    ConsoleIO.Display(puzzle);
                    puzzleSolver.Solve(1);
                    Console.WriteLine();
                    Console.WriteLine("============= new answers ============= ");
                    ConsoleIO.Display(puzzle, clear: false);


                }
            }
        }
    }
}
