﻿using System.Collections.Generic;

namespace Sudoku.Core
{
    public class PuzzleSpot
    {
        private int? _answer;

        public int? Answer 
        { 
            get => _answer; 
            set 
            {
                if (value.HasValue && value.Value > 0 && value.Value < 10)
                {
                    _answer = value;
                }
                else
                {
                    _answer = null;
                }
            } 
        }
        public HashSet<int> Possibilities { get; set; }

        public PuzzleSpot()
        {
            Possibilities = new HashSet<int>();
        }

        public PuzzleSpot(int? value)
        {
            if (value.HasValue && (value.Value < 1 || value.Value > 9))
            {
                value = null;
            }

            Answer = value;
            Possibilities = new HashSet<int>();
        }
    }
}
