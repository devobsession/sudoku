﻿using System.Collections.Generic;

namespace Sudoku.Core
{
    public class Move
    {
        public int X { get; set; }
        public int Y { get; set; }
        public List<RemovedPossibility> RemovedPossibilities { get; set; }
        public int? Answer { get; set; }
        public Move(int y, int x, int? answer)
        {
            Y = y;
            X = x;
            Answer = answer;
            RemovedPossibilities = new List<RemovedPossibility>();
        }

        public Move()
        {
            RemovedPossibilities = new List<RemovedPossibility>();
        }
    }
}
