﻿namespace Sudoku.Core
{
    public class Coordinates
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Coordinates(int y, int x)
        {
            Y = y;
            X = x;
        }

    }
}
