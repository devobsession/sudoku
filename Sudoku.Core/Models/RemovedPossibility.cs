﻿namespace Sudoku.Core
{
    public class RemovedPossibility
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Possibility { get; set; }

        public RemovedPossibility(int y, int x, int possibility)
        {
            Y = y;
            X = x;
            Possibility = possibility;
        }
    }
}
