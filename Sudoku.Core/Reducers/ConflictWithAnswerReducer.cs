﻿using System.Collections.Generic;
using System.Linq;

namespace Sudoku.Core
{
    public class ConflictWithAnswerReducer : IReducer
    {
        private Puzzle _puzzle;

        public Puzzle Reduce(Puzzle puzzle)
        {
            _puzzle = puzzle;

            for (int y = 0; y < _puzzle.Board.GetLength(0); y++)
            {
                for (int x = 0; x < _puzzle.Board.GetLength(1); x++)
                {
                    if (_puzzle.Board[y, x].Answer.HasValue)
                    {
                        if (_puzzle.Board[y, x].Possibilities.Any())
                        {
                            _puzzle.Board[y, x].Possibilities = new HashSet<int>();
                        }
                        
                        ReduceColumnPossibilities(x, _puzzle.Board[y, x].Answer.Value);
                        ReduceRowPossibilities(y, _puzzle.Board[y, x].Answer.Value);
                        ReduceBlockPossibilities(y, x, _puzzle.Board[y, x].Answer.Value);
                    }
                }
            }

            return _puzzle;
        }

        private void ReduceColumnPossibilities(int x, int value)
        {
            for (int i = 0; i < 9; i++)
            {
                _puzzle.Board[i, x].Possibilities.Remove(value);
            }
        }

        private void ReduceRowPossibilities(int y, int value)
        {
            for (int i = 0; i < 9; i++)
            {
                _puzzle.Board[y, i].Possibilities.Remove(value);
            }
        }

        private void ReduceBlockPossibilities(int y, int x, int value)
        {
            var newX = _puzzle.GetClosestBlockCenter(x);
            var newY = _puzzle.GetClosestBlockCenter(y);

            _puzzle.Board[newY - 1, newX - 1].Possibilities.Remove(value);
            _puzzle.Board[newY - 1, newX].Possibilities.Remove(value);
            _puzzle.Board[newY - 1, newX + 1].Possibilities.Remove(value);
            _puzzle.Board[newY, newX - 1].Possibilities.Remove(value);
            _puzzle.Board[newY, newX].Possibilities.Remove(value);
            _puzzle.Board[newY, newX + 1].Possibilities.Remove(value);
            _puzzle.Board[newY + 1, newX - 1].Possibilities.Remove(value);
            _puzzle.Board[newY + 1, newX].Possibilities.Remove(value);
            _puzzle.Board[newY + 1, newX + 1].Possibilities.Remove(value);
        }
    }

}
