﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku.Core
{
    /// <summary>
    /// if x number of blocks have x number of the same number, and they do not exist anywhere else, then remove all other possibilities within those blocks
    /// </summary>
    public class NumberOfBlocksWithSameNumberOfPossibilitiesReducer : IReducer
    {
        private Puzzle _puzzle;

        public Puzzle Reduce(Puzzle puzzle)
        {
            _puzzle = puzzle;

            ReduceBlock(1, 1);
            ReduceBlock(1, 4);
            ReduceBlock(1, 7);

            ReduceBlock(4, 1);
            ReduceBlock(4, 4);
            ReduceBlock(4, 7);

            ReduceBlock(7, 1);
            ReduceBlock(7, 4);
            ReduceBlock(7, 7);

            return puzzle;
        }


        private void ReduceBlock(int y, int x)
        {
            var blocksOnlyOptions = GetBlocksOnlyOptions(y, x);
            foreach (var coordinates in blocksOnlyOptions.Coordinates)
            {
                _puzzle.Board[coordinates.Y, coordinates.X].Possibilities.RemoveWhere(a => blocksOnlyOptions.PossibilitiesToKeep.All(b => a != b));
            }
        }

        private BlocksOnlyOptions GetBlocksOnlyOptions(int y, int x)
        {
            var newX = _puzzle.GetClosestBlockCenter(x);
            var newY = _puzzle.GetClosestBlockCenter(y);

            Dictionary<int, List<int>> possibilityToSpotNumbers = new Dictionary<int, List<int>>()
            {
                { 1, new List<int>() },
                { 2, new List<int>() },
                { 3, new List<int>() },
                { 4, new List<int>() },
                { 5, new List<int>() },
                { 6, new List<int>() },
                { 7, new List<int>() },
                { 8, new List<int>() },
                { 9, new List<int>() }
            };

            for (int i = 1; i <= 9; i++)
            {
                if (_puzzle.Board[newY - 1, newX -1].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(1);
                }
                if (_puzzle.Board[newY - 1, newX].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(2);
                }
                if (_puzzle.Board[newY - 1, newX + 1].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(3);
                }

                if (_puzzle.Board[newY, newX - 1].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(4);
                }
                if (_puzzle.Board[newY, newX].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(5);
                }
                if (_puzzle.Board[newY, newX + 1].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(6);
                }

                if (_puzzle.Board[newY + 1, newX - 1].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(7);
                }
                if (_puzzle.Board[newY + 1, newX].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(8);
                }
                if (_puzzle.Board[newY + 1, newX + 1].Possibilities.Contains(i))
                {
                    possibilityToSpotNumbers[i].Add(9);
                }
            }

            var blocksOnlyOptions = new BlocksOnlyOptions();
            foreach (var possibilityToSpotNumber in possibilityToSpotNumbers)
            {
                var otherSameGroups = possibilityToSpotNumbers.Where(x => x.Key != possibilityToSpotNumber.Key && x.Value.Count == possibilityToSpotNumber.Value.Count && x.Value.All(z => possibilityToSpotNumber.Value.Any(y => z == y))).ToList();
                
                if (otherSameGroups.Count + 1 == possibilityToSpotNumber.Value.Count)
                {
                    blocksOnlyOptions.PossibilitiesToKeep.Add(possibilityToSpotNumber.Key);
                    foreach (var otherSameGroup in otherSameGroups)
                    {
                        blocksOnlyOptions.PossibilitiesToKeep.Add(otherSameGroup.Key);
                    }

                    foreach (var spotNumber in possibilityToSpotNumber.Value)
                    {
                        var coordinates = GetCoordinatesFromSpotNumber(newY, newX, spotNumber);
                        blocksOnlyOptions.AddCoordinates(coordinates);
                    }
                }
            }

            return blocksOnlyOptions;
        }

        private Coordinates GetCoordinatesFromSpotNumber(int centreY, int centreX, int spotNumber)
        {
            switch (spotNumber)
            {
                case 1:
                    return new Coordinates(centreY-1, centreX-1);
                case 2:
                    return new Coordinates(centreY - 1, centreX);
                case 3:
                    return new Coordinates(centreY - 1, centreX + 1);
                case 4:
                    return new Coordinates(centreY, centreX - 1);
                case 5:
                    return new Coordinates(centreY, centreX);
                case 6:
                    return new Coordinates(centreY, centreX + 1);
                case 7:
                    return new Coordinates(centreY+1, centreX - 1);
                case 8:
                    return new Coordinates(centreY + 1, centreX);
                case 9:
                    return new Coordinates(centreY + 1, centreX + 1);
                default:
                    return null;
            }

        }

    }


    public class BlocksOnlyOptions
    {
        public List<Coordinates> Coordinates { get; set; }
        public HashSet<int> PossibilitiesToKeep { get; set; }

        public BlocksOnlyOptions()
        {
            Coordinates = new List<Coordinates>();
            PossibilitiesToKeep = new HashSet<int>();
        }

        public void AddCoordinates(Coordinates coordinates)
        {
            if (Coordinates.Any(x => x.X == coordinates.X && x.Y == coordinates.Y) == false)
            {
                Coordinates.Add(coordinates);
            }
        }
    }
}
