﻿using System.Collections.Generic;
using System.Linq;

namespace Sudoku.Core
{
    public class StraightLinePossibilityReducer : IReducer
    {
        private Puzzle _puzzle;

        public Puzzle Reduce(Puzzle puzzle)
        {
            _puzzle = puzzle;

            CheckAndReduceBlock(1, 1);
            CheckAndReduceBlock(1, 4);
            CheckAndReduceBlock(1, 7);
            CheckAndReduceBlock(4, 1);
            CheckAndReduceBlock(4, 4);
            CheckAndReduceBlock(4, 7);
            CheckAndReduceBlock(7, 1);
            CheckAndReduceBlock(7, 4);
            CheckAndReduceBlock(7, 7);

            for (int y = 0; y < _puzzle.Board.GetLength(0); y++)
            {
                for (int x = 0; x < _puzzle.Board.GetLength(1); x++)
                {
                    if (_puzzle.Board[y, x].Answer.HasValue)
                    {

                    }
                }
            }

            return _puzzle;
        }

        private void CheckAndReduceBlock(int centreY, int centreX)
        {
            var columns = new List<int>() { centreX - 1, centreX, centreX + 1 };
            var rows = new List<int>() { centreY - 1, centreY, centreY + 1 };

            var straightLineRows = GetBlocksStraightLinePossibilitiesInRow(centreY, centreX);
            if (straightLineRows.Any())
            {
                foreach (var straightLineRow in straightLineRows)
                {
                    ReduceRowStraightLinePossibility(straightLineRow.Row, straightLineRow.Possibility, columns);
                }
            }

            var straightLineColumns = GetBlocksStraightLinePossibilitiesInColumn(centreY, centreX);
            if (straightLineColumns.Any())
            {
                foreach (var straightLineColumn in straightLineColumns)
                {
                    ReduceColumnStraightLinePossibility(straightLineColumn.Column, straightLineColumn.Possibility, rows);
                }
            }
        }

        private List<StraightLineRow> GetBlocksStraightLinePossibilitiesInRow(int centreY, int centreX)
        {
            var straightLineRows = new List<StraightLineRow>();

            for (int i = 1; i <= 9; i++)
            {
                var row1_1 = _puzzle.Board[centreY - 1, centreX - 1].Possibilities.Contains(i);
                var row1_2 = _puzzle.Board[centreY - 1, centreX].Possibilities.Contains(i);
                var row1_3 = _puzzle.Board[centreY - 1, centreX + 1].Possibilities.Contains(i);

                var row2_1 = _puzzle.Board[centreY, centreX - 1].Possibilities.Contains(i);
                var row2_2 = _puzzle.Board[centreY, centreX].Possibilities.Contains(i);
                var row2_3 = _puzzle.Board[centreY, centreX + 1].Possibilities.Contains(i);

                var row3_1 = _puzzle.Board[centreY + 1, centreX - 1].Possibilities.Contains(i);
                var row3_2 = _puzzle.Board[centreY + 1, centreX].Possibilities.Contains(i);
                var row3_3 = _puzzle.Board[centreY + 1, centreX + 1].Possibilities.Contains(i);

                if ((row1_1 || row1_2 || row1_3) && !row2_1 && !row2_2 && !row2_3 && !row3_1 && !row3_2 && !row3_3)
                {
                    straightLineRows.Add(new StraightLineRow(centreY-1, i));
                }
                else if ((row2_1 || row2_2 || row2_3) && !row1_1 && !row1_2 && !row1_3 && !row3_1 && !row3_2 && !row3_3)
                {
                    straightLineRows.Add(new StraightLineRow(centreY, i));
                }
                else if ((row3_1 || row3_2 || row3_3) && !row2_1 && !row2_2 && !row2_3 && !row1_1 && !row1_2 && !row1_3)
                {
                    straightLineRows.Add(new StraightLineRow(centreY + 1, i));
                }
            }

            return straightLineRows;
        }

        private void ReduceRowStraightLinePossibility(int row, int possibility, List<int> spotExceptions = null)
        {
            for (int i = 0; i < 9; i++)
            {
                if (spotExceptions != null && spotExceptions.Contains(i))
                {
                    continue;
                }

                _puzzle.Board[row, i].Possibilities.Remove(possibility);
            }
        }

        private List<StraightLineColumn> GetBlocksStraightLinePossibilitiesInColumn(int centreY, int centreX)
        {
            var straightLineColumns = new List<StraightLineColumn>();

            for (int i = 1; i <= 9; i++)
            {
                var col1_1 = _puzzle.Board[centreY - 1, centreX - 1].Possibilities.Contains(i);
                var col1_2 = _puzzle.Board[centreY, centreX - 1].Possibilities.Contains(i);
                var col1_3 = _puzzle.Board[centreY + 1, centreX - 1].Possibilities.Contains(i);

                var col2_1 = _puzzle.Board[centreY - 1, centreX].Possibilities.Contains(i);
                var col2_2 = _puzzle.Board[centreY, centreX].Possibilities.Contains(i);
                var col2_3 = _puzzle.Board[centreY + 1, centreX].Possibilities.Contains(i);

                var col3_1 = _puzzle.Board[centreY - 1, centreX + 1].Possibilities.Contains(i);
                var col3_2 = _puzzle.Board[centreY, centreX + 1].Possibilities.Contains(i);
                var col3_3 = _puzzle.Board[centreY + 1, centreX + 1].Possibilities.Contains(i);

                if ((col1_1 || col1_2 || col1_3) && !col2_1 && !col2_2 && !col2_3 && !col3_1 && !col3_2 && !col3_3)
                {
                    straightLineColumns.Add(new StraightLineColumn(centreX - 1, i));
                }
                else if ((col2_1 || col2_2 || col2_3) && !col1_1 && !col1_2 && !col1_3 && !col3_1 && !col3_2 && !col3_3)
                {
                    straightLineColumns.Add(new StraightLineColumn(centreX, i));
                }
                else if ((col3_1 || col3_2 || col3_3) && !col2_1 && !col2_2 && !col2_3 && !col1_1 && !col1_2 && !col1_3)
                {
                    straightLineColumns.Add(new StraightLineColumn(centreX + 1, i));
                }
            }

            return straightLineColumns;
        }

        private void ReduceColumnStraightLinePossibility(int column, int possibility, List<int> spotExceptions = null)
        {
            for (int i = 0; i < 9; i++)
            {
                if (spotExceptions != null && spotExceptions.Contains(i))
                {
                    continue;
                }

                _puzzle.Board[i, column].Possibilities.Remove(possibility);
            }
        }

        public class StraightLineRow
        {
            public int Row { get; set; }
            public int Possibility { get; set; }

            public StraightLineRow(int row, int possibility)
            {
                Row = row;
                Possibility = possibility;
            }
        }

        public class StraightLineColumn
        {
            public int Column { get; set; }
            public int Possibility { get; set; }

            public StraightLineColumn(int column, int possibility)
            {
                Column = column;
                Possibility = possibility;
            }

        }
    }

}
