﻿namespace Sudoku.Core
{
    public interface IReducer
    {
        Puzzle Reduce(Puzzle puzzle);
    }
}