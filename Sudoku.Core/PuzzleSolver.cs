﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku.Core
{
    public class PuzzleSolver
    {
        private Puzzle _puzzle;
        private readonly List<IReducer> _reducers;
        private bool _possibilitiesInitiated;
        public PuzzleSolver(Puzzle puzzle, List<IReducer> reducers)
        {
            _puzzle = puzzle;
            _reducers = reducers;
            _possibilitiesInitiated = false;
        }

        public void Solve(int iterations)
        {
            if (_possibilitiesInitiated == false)
            {
                InitiateAllPossibilities();
            }

            for (int i = 0; i < iterations; i++)
            {
                foreach (var reducer in _reducers)
                {
                    _puzzle = reducer.Reduce(_puzzle);
                }

                AnswerWhereOnlyPossibilityOnSpot();
                AnswerWhereOnlyPossibilityInEachBlock();
                AnswerWhereOnlyPossibilityInEachRow();
                AnswerWhereOnlyPossibilityInEachColumn();
            }
        }

        private void AnswerWhereOnlyPossibilityOnSpot()
        {
            for (int y = 0; y < _puzzle.Board.GetLength(0); y++)
            {
                for (int x = 0; x < _puzzle.Board.GetLength(1); x++)
                {
                    if (_puzzle.Board[y, x].Answer == null || _puzzle.Board[y, x].Answer.Value == 0)
                    {
                        if (_puzzle.Board[y, x].Possibilities.Count == 1)
                        {
                            _puzzle.SetAnswer(y, x, _puzzle.Board[y, x].Possibilities.Single());
                        }
                    }
                }
            }
        }

        private void AnswerWhereOnlyPossibilityInEachBlock()
        {
            for (int y = 1; y < 8; y+=3)
            {
                for (int x = 1; x < 8; x += 3)
                {
                    AnswerWhereOnlyPossibilityInBlock(y, x);
                }
            }
        }

        private void AnswerWhereOnlyPossibilityInEachRow()
        {
            for (int y = 0; y < 9; y++)
            {
                AnswerWhereOnlyPossibilityInRow(y);
            }
        }

        private void AnswerWhereOnlyPossibilityInEachColumn()
        {
            for (int x = 0; x < 9; x++)
            {
                AnswerWhereOnlyPossibilityInColumn(x);
            }
        }

        private void AnswerWhereOnlyPossibilityInBlock(int y, int x)
        {
            var newX = _puzzle.GetClosestBlockCenter(x);
            var newY = _puzzle.GetClosestBlockCenter(y);

            for (int i = 1; i <= 9; i++)
            {
                var found = new List<Coordinates>();

                AddToListIfContainsPossibility(newY-1, newX-1, i, found);
                AddToListIfContainsPossibility(newY-1, newX, i, found);
                AddToListIfContainsPossibility(newY-1, newX+1, i, found);

                AddToListIfContainsPossibility(newY, newX-1, i, found);
                AddToListIfContainsPossibility(newY, newX, i, found);
                AddToListIfContainsPossibility(newY, newX+1, i, found);

                AddToListIfContainsPossibility(newY+1, newX-1, i, found);
                AddToListIfContainsPossibility(newY+1, newX, i, found);
                AddToListIfContainsPossibility(newY+1, newX+1, i, found);

                if (found.Count == 1)
                {
                    var answerCoordinates = found.Single();
                    _puzzle.SetAnswer(answerCoordinates.Y, answerCoordinates.X, i);
                }
            }
        }

        private void AnswerWhereOnlyPossibilityInRow(int y)
        {
            for (int i = 1; i <= 9; i++)
            {
                var found = new List<Coordinates>();

                for (int x = 0; x < 9; x++)
                {
                    AddToListIfContainsPossibility(y, x, i, found);
                }
                
                if (found.Count == 1)
                {
                    var answerCoordinates = found.Single();
                    _puzzle.SetAnswer(answerCoordinates.Y, answerCoordinates.X, i);
                }
            }
        }

        private void AnswerWhereOnlyPossibilityInColumn(int x)
        {
            for (int i = 1; i <= 9; i++)
            {
                var found = new List<Coordinates>();

                for (int y = 0; y < 9; y++)
                {
                    AddToListIfContainsPossibility(y, x, i, found);
                }

                if (found.Count == 1)
                {
                    var answerCoordinates = found.Single();
                    _puzzle.SetAnswer(answerCoordinates.Y, answerCoordinates.X, i);
                }
            }
        }

        private void AddToListIfContainsPossibility(int y, int x, int value, List<Coordinates> found)
        {
            var coordinates = new Coordinates(y, x);
            if (_puzzle.Board[coordinates.Y, coordinates.X].Possibilities.Contains(value))
            {
                found.Add(coordinates);
            }
        }

        public void InitiateAllPossibilities()
        {
            _possibilitiesInitiated = true;

            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    if (_puzzle.Board[y, x].Answer == null)
                    {
                        _puzzle.Board[y, x].Possibilities = Enumerable.Range(1, 9).ToHashSet();
                    }
                }
            }
        }

    }

}
