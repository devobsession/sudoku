﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku.Core
{
    public class Puzzle
    {
        public PuzzleSpot[,] Board;
        private Stack<Move> _moves;

        public Puzzle(int[] puzzleArr)
        {
            InitiateEmptyBoard();

            SetBoard(puzzleArr);
        }

        public Puzzle()
        {
            InitiateEmptyBoard();
        }

        public Puzzle(int puzzleNumber)
        {
            InitiateEmptyBoard();

            if (puzzleNumber < PuzzleLoader.Puzzles.Count)
            {
                var puzzleArr = PuzzleLoader.Puzzles[puzzleNumber];
                SetBoard(puzzleArr);
            }            
        }

        public void Randomize(int difficulty)
        {
            if (difficulty < 10)
            {
                difficulty = 10;
            }

            if (difficulty > 70)
            {
                difficulty = 70;
            }

            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    Board[y, x].Possibilities = new HashSet<int>(){1, 2, 3, 4, 5, 6, 7, 8, 9};
                }
            }

            Random r = new Random();
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    if (Board[y, x].Possibilities.Count == 0)
                    {
                        for (int i = 0; i < 40; i++)
                        {
                            var undoneMove = Undo();
                            if (undoneMove == null)
                            {
                                break;
                            }
                            else
                            {
                                y = undoneMove.Y;
                                x = undoneMove.X;
                            }
                        }

                        x--;
                        continue;
                    }
                    else
                    {
                        var index = r.Next(Board[y, x].Possibilities.Count);
                        var answer = Board[y, x].Possibilities.ToList()[index];
                        SetAnswer(y, x, answer);
                    }                    
                }
            }

            for (int i = 0; i < difficulty; i++)
            {
                for (int n = 0; n < 10; n++)
                {
                    var randomSpot = r.Next(81);
                    var y = randomSpot / 9;
                    var x = randomSpot % 9;

                    if (Board[y, x].Answer != null)
                    {
                        Board[y, x].Answer = null;
                        break;
                    }                    
                }    
            }

            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    Board[y, x].Possibilities = new HashSet<int>();
                }
            }
        }

        public bool SetAnswer(int y, int x, int answer)
        {
            if ((Board[y, x].Answer == null || Board[y, x].Answer == 0) && IsNewAnswerAllowed(y, x, answer))
            {
                var move = new Move(y, x, answer);
                ReducePossibilities(move);
                Board[y, x].Answer = answer;
                _moves.Push(move);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Move Undo()
        {
            if (_moves != null && _moves.Any())
            {
                var move = _moves.Pop();
                Board[move.Y, move.X].Answer = null;

                if (move.RemovedPossibilities != null && move.RemovedPossibilities.Any())
                {
                    foreach (var removedPossibility in move.RemovedPossibilities)
                    {
                        Board[removedPossibility.Y, removedPossibility.X].Possibilities.Add(removedPossibility.Possibility);
                    }
                }

                return move;
            }

            return null;
        }

        public List<int> GetPossibilities(int y, int x)
        {
            return Board[y, x].Possibilities.ToList();
        }

        public void AddPossibility(int y, int x, int possibility)
        {
            Board[y, x].Possibilities.Add(possibility);
        }

        public void RemovePossibility(int y, int x, int possibility)
        {
            Board[y, x].Possibilities.Remove(possibility);
        }

        public void ReducePossibilities(Move move)
        {
            ReduceColumnPossibilities(move);
            ReduceRowPossibilities(move);
            ReduceBlockPossibilities(move);
        }

        public void ReduceColumnPossibilities(Move move)
        {
            for (int i = 0; i < 9; i++)
            {
                RemovePossibility(move, i, move.X);
            }
        }

        private void ReduceRowPossibilities(Move move)
        {
            for (int i = 0; i < 9; i++)
            {
                RemovePossibility(move, move.Y, i);
            }
        }

        private void ReduceBlockPossibilities(Move move)
        {
            var newX = GetClosestBlockCenter(move.X);
            var newY = GetClosestBlockCenter(move.Y);

            RemovePossibility(move, newY - 1, newX - 1);
            RemovePossibility(move, newY - 1, newX);
            RemovePossibility(move, newY - 1, newX + 1);
            RemovePossibility(move, newY, newX - 1);
            RemovePossibility(move, newY, newX);
            RemovePossibility(move, newY, newX + 1);
            RemovePossibility(move, newY + 1, newX - 1);
            RemovePossibility(move, newY + 1, newX);
            RemovePossibility(move, newY + 1, newX + 1);
        }

        private void RemovePossibility(Move move, int y, int x)
        {
            var exists = Board[y, x].Possibilities.Remove(move.Answer.Value);
            if (exists)
            {
                move.RemovedPossibilities.Add(new RemovedPossibility(y, x, move.Answer.Value));
            }
        }

        public bool IsNewAnswerAllowed(int y, int x, int value)
        {
            if (value == 0)
            {
                return true;
            }

            return IsAllowedInColumn(x, value) && IsAllowedInRow(y, value) && IsAllowedInBlock(y, x, value);
        }

        public bool IsAllowedInColumn(int x, int value)
        {
            if (value == 0)
            {
                return true;
            }

            for (int i = 0; i < 9; i++)
            {
                if (IsSpotValid(i, x, value) == false)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsAllowedInRow(int y, int value)
        {
            if (value == 0)
            {
                return true;
            }

            for (int i = 0; i < 9; i++)
            {
                if (IsSpotValid(y, i, value) == false)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsAllowedInBlock(int y, int x, int value)
        {
            if (value == 0)
            {
                return true;
            }

            var newX = GetClosestBlockCenter(x);
            var newY = GetClosestBlockCenter(y);

            return IsSpotValid(newY - 1, newX - 1, value) &&
                   IsSpotValid(newY - 1, newX, value) &&
                   IsSpotValid(newY - 1, newX + 1, value) &&
                   IsSpotValid(newY, newX - 1, value) &&
                   IsSpotValid(newY, newX, value) &&
                   IsSpotValid(newY, newX + 1, value) &&
                   IsSpotValid(newY + 1, newX - 1, value) &&
                   IsSpotValid(newY + 1, newX, value) &&
                   IsSpotValid(newY + 1, newX + 1, value);
        }

        private bool IsSpotValid(int y, int x, int value)
        {
            if (Board[y, x].Answer.HasValue && Board[y, x].Answer.Value == value)
            {
                return false;
            }

            return true;
        }

        public int GetClosestBlockCenter(int value)
        {
            if (value <= 2)
            {
                return 1;
            }
            else if (value <= 5)
            {
                return 4;
            }
            else if (value <= 8)
            {
                return 7;
            }
            else
            {
                return 0;
            }
        }

        private void InitiateEmptyBoard()
        {
            _moves = new Stack<Move>();
            Board = new PuzzleSpot[9, 9];

            for (int y = 0; y < Board.GetLength(0); y++)
            {
                for (int x = 0; x < Board.GetLength(1); x++)
                {
                    if (Board[y, x] == null)
                    {
                        Board[y, x] = new PuzzleSpot();
                    }
                }
            }
        }

        private void SetBoard(int[] puzzleArr)
        {
            if (puzzleArr.Count() != 81)
            {
                throw new Exception("Incorrect Array");
            }

            for (int y = 0; y < Board.GetLength(0); y++)
            {
                for (int x = 0; x < Board.GetLength(1); x++)
                {
                    var value = puzzleArr[(y * 9) + x];
                    if (IsNewAnswerAllowed(y, x, value) == false)
                    {
                        throw new Exception($"Invalid board: x={x}, y={y}, value={value}");
                    }
                    Board[y, x].Answer = value;
                }
            }
        }
    }
}
